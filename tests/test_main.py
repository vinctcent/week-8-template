from unittest.mock import Mock

from csuibot.main import help, zodiac, shio, dayofdate, dayofdate_invalid_input


def test_help(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mock_message = Mock()

    help(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (
        'CSUIBot v0.0.1\n\n'
        'Dari Fasilkom, oleh Fasilkom, untuk Fasilkom!'
    )
    assert args[1] == expected_text


def test_zodiac(mocker):
    fake_zodiac = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_zodiac', return_value=fake_zodiac)
    mock_message = Mock(text='/zodiac 2015-05-05')

    zodiac(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_zodiac


def test_zodiac_invalid_month_or_day(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/zodiac 2015-25-05')

    zodiac(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Month or day is invalid'


def test_shio(mocker):
    fake_shio = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_chinese_zodiac', return_value=fake_shio)
    mock_message = Mock(text='/shio 2015-05-05')

    shio(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_shio


def test_shio_invalid_year(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.lookup_chinese_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/shio 1134-05-05')

    shio(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Year is invalid'


def test_dayofdate(mocker):
    dayname = 'Friday'
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.dayofdate', return_value=dayname)
    mock_message = Mock(text='/dayofdate 2017-05-05')

    dayofdate(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Friday'


def test_dayofdate_invalid_year(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.dayofdate', side_effect=ValueError)
    mock_message = Mock(text='/dayofdate 0000-05-05')

    dayofdate(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Incorrect use of dayofdate command.\
    Please write a valid date in the form of yyyy-mm-dd, such as 2016-05-13'


def test_dayofdate_invalid_month(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.dayofdate', side_effect=ValueError)
    mock_message = Mock(text='/dayofdate 2017-14-05')

    dayofdate(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Incorrect use of dayofdate command.\
    Please write a valid date in the form of yyyy-mm-dd, such as 2016-05-13'


def test_dayofdate_invalid_day(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.dayofdate', side_effect=ValueError)
    mock_message = Mock(text='/dayofdate 2016-12-43')

    dayofdate(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Incorrect use of dayofdate command.\
    Please write a valid date in the form of yyyy-mm-dd, such as 2016-05-13'


def test_dayofdate_using_words(mocker):
    mocked_reply_to = mocker.patch('csuibot.main.bot.reply_to')
    mocker.patch('csuibot.main.dayofdate_invalid_input')
    mock_message = Mock(text='/dayofdate foobar')

    dayofdate_invalid_input(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Incorrect use of dayofdate command.\
    Please write a valid date in the form of yyyy-mm-dd, such as 2016-05-13'
