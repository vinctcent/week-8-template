from datetime import date


def lookup_zodiac(month, day):
    zodiacs = [
        Aries(),
        # Implement the mandatory task in here
        Taurus(),
        Gemini(),
        Cancer(),
        Leo(),
        Virgo(),
        Libra(),
        Scorpio(),
        Sagittarius(),
        Capricorn(),
        Aquarius(),
        Pisces()
    ]

    for zodiac in zodiacs:
        if zodiac.date_includes(month, day):
            return zodiac.name
    else:
        return 'Unknown zodiac'


def lookup_chinese_zodiac(year):
    num_zodiacs = 12
    zodiacs = {
        0: 'rat',
        1: 'buffalo',
        2: 'tiger',
        3: 'rabbit',
        4: 'dragon',
        5: 'snake',
        6: 'horse',
        7: 'goat',
        8: 'monkey',
        9: 'rooster',
        10: 'dog',
        11: 'pig'
        # Implement the mandatory task in here
    }
    ix = (year - 4) % num_zodiacs

    try:
        return zodiacs[ix]
    except KeyError:
        return 'Unknown chinese zodiac'


def checkday(year, month, day):
    return date(year, month, day).strftime("%A")


class Zodiac:

    def make_date(self, month, day, year=2000):
        return date(year, month, day)

    def date_includes(self, month, day):
        _date = self.make_date(month, day)
        return self.lower_bound <= _date <= self.upper_bound


class Aries(Zodiac):

    def __init__(self):
        self.name = 'aries'
        self.lower_bound = self.make_date(3, 21)
        self.upper_bound = self.make_date(4, 19)


class Taurus(Zodiac):

    def __init__(self):
        self.name = 'taurus'
        self.lower_bound = self.make_date(4, 20)
        self.upper_bound = self.make_date(5, 20)


class Gemini(Zodiac):

    def __init__(self):
        self.name = 'gemini'
        self.lower_bound = self.make_date(5, 21)
        self.upper_bound = self.make_date(6, 20)


class Cancer(Zodiac):

    def __init__(self):
        self.name = 'cancer'
        self.lower_bound = self.make_date(6, 21)
        self.upper_bound = self.make_date(7, 22)


class Leo(Zodiac):

    def __init__(self):
        self.name = 'leo'
        self.lower_bound = self.make_date(7, 23)
        self.upper_bound = self.make_date(8, 22)


class Virgo(Zodiac):

    def __init__(self):
        self.name = 'virgo'
        self.lower_bound = self.make_date(8, 23)
        self.upper_bound = self.make_date(9, 22)


class Libra(Zodiac):

    def __init__(self):
        self.name = 'libra'
        self.lower_bound = self.make_date(9, 23)
        self.upper_bound = self.make_date(10, 22)


class Scorpio(Zodiac):

    def __init__(self):
        self.name = 'scorpio'
        self.lower_bound = self.make_date(10, 23)
        self.upper_bound = self.make_date(11, 21)


class Sagittarius(Zodiac):

    def __init__(self):
        self.name = 'sagittarius'
        self.lower_bound = self.make_date(11, 22)
        self.upper_bound = self.make_date(12, 21)


class Capricorn(Zodiac):

    def __init__(self):
        self.name = 'capricorn'
        self.lower_bound = self.make_date(12, 22, year=2000)
        self.upper_bound = self.make_date(1, 19, year=2001)

    def date_includes(self, month, day):
        year = 2000 if month == 12 else 2001
        _date = self.make_date(month, day, year=year)
        return self.lower_bound <= _date <= self.upper_bound


class Aquarius(Zodiac):

    def __init__(self):
        self.name = 'aquarius'
        self.lower_bound = self.make_date(1, 20)
        self.upper_bound = self.make_date(2, 18)


class Pisces(Zodiac):

    def __init__(self):
        self.name = 'pisces'
        self.lower_bound = self.make_date(2, 19)
        self.upper_bound = self.make_date(3, 20)


class Rat(Zodiac):

    def __init__(self):
        self.name = 'rat'
        self.years = [1996, 1984, 1972, 1960, 2008, 2020]


class Buffalo(Zodiac):

    def __init__(self):
        self.name = 'buffalo'
        self.years = [1997, 1985, 1973, 1961, 2009, 2021]


class Tiger(Zodiac):

    def __init__(self):
        self.name = 'tiger'
        self.years = [1998, 1986, 1974, 1962, 2010, 2022]


class Rabbit(Zodiac):

    def __init__(self):
        self.name = 'rabbit'
        self.years = [1999, 1987, 1975, 1963, 2011, 2023]


class Dragon(Zodiac):

    def __init__(self):
        self.name = 'dragon'
        self.years = [2000, 1988, 1976, 1964, 2012, 2024]


class Snake(Zodiac):

    def __init__(self):
        self.name = 'snake'
        self.years = [2001, 1989, 1977, 1965, 2013, 2025]


class Horse(Zodiac):

    def __init__(self):
        self.name = 'horse'
        self.years = [2002, 1990, 1978, 1966, 2014, 2026]


class Goat(Zodiac):

    def __init__(self):
        self.name = 'goat'
        self.years = [2003, 1991, 1979, 1967, 2015, 2027]


class Monkey(Zodiac):

    def __init__(self):
        self.name = 'monkey'
        self.years = [2004, 1992, 1980, 1968, 2016, 2028]


class Rooster(Zodiac):

    def __init__(self):
        self.name = 'rooster'
        self.years = [2005, 1993, 1981, 1969, 2017, 2029]


class Dog(Zodiac):

    def __init__(self):
        self.name = 'dog'
        self.years = [2006, 1994, 1982, 1970, 2018, 2030]


class Pig(Zodiac):

    def __init__(self):
        self.name = 'pig'
        self.years = [2007, 1995, 1983, 1971, 2019, 2031]
